from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('',include('social.apps.django_app.urls',namespace='social')),
    path('admin/', admin.site.urls),
    path('', include('misgatis.urls')),
]
