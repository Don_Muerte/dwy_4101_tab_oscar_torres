from django.db import models

class Gato(models.Model):
    nombre_gato = models.TextField(null=False,blank=False)
    edad = models.TextField(null=False,blank=False)
    duenio_nombre = models.TextField(null=False,blank=False)
    duenio_apellido = models.TextField(null=False,blank=False)
    correo = models.TextField(null=False,blank=False)
    ciudad = models.TextField(null=False,blank=False)
    region = models.TextField(null=False,blank=False)
    telefono = models.TextField(null=False,blank=False)