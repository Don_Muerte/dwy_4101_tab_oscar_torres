from django import forms
from misgatis.models import Gato

class GatoForm(forms.ModelForm):

    class Meta:
        model = Gato
        fields = ('nombre_gato','edad','duenio_nombre','duenio_apellido','correo','ciudad','region','telefono',)